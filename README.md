# TinyBuck
### A 25.4x12.7 mm big TPS62135 based buck converter
Requires [this library](https://github.com/fullyautomated/means-of-circuit-design)


# Specs
- 4 A output current
- 3 to 17 V input voltage
- 0 to 12 V output voltage
- Remote output voltage control
- Remote on/off
- Remote output voltage sensing to compensate for voltage drop on cables



# Pictures

![top view](front.png)

![sideways view](side.png)

![bottom view](back.png)

![schematic layer 1](sch_front.png)

![schematic layer 2](sch_back.png)
